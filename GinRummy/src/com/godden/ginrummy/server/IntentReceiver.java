package com.godden.ginrummy.server;

import com.godden.ginrummy.interfaces.IReceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * An extension of the Broadcast Reciever for binders to recieve messages from ElectroServer
 * @author Joshua Godden
 * @version 1.0
 */
public class IntentReceiver extends BroadcastReceiver {

	private IReceiver _receiver;
	
	public IntentReceiver(IReceiver receiver) {
		_receiver = receiver;
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		_receiver.receivedBroadcast(intent);
	}
}
