package com.godden.ginrummy.server.json;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;

import android.util.Log;

import com.godden.ginrummy.client.ClientCard;
import com.godden.ginrummy.client.ClientGame;
import com.godden.ginrummy.client.ClientGlobal;

/**
 * A static class which handles JSON Communications from serializing game data to decoding data
 * @author Joshua Godden
 * @version 1.0
 */
public final class JSONComms {
	
	/**
	 * Serializes a lobby message
	 * @param status	The status of the lobby
	 * @return	Returns an encoded JSON String of the lobby
	 */
	public static String serializeLobby(JSONLobbyArgs.Status status) {
		JSONObject obj = new JSONObject();
		try {
			obj.put("meta", 	"lobby");
			obj.put("status", 	status.getValue());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return obj.toString();
	}
	
	/**
	 * Serializes a game message
	 * @param state	The current state of play
	 * @param type	The type of message (deprecated)
	 * @param game	The game model that will be used to build a message
	 * @return	Returns an encoded JSON String of the game
	 */
	public static String serializeGame(JSONGameArgs.State state,
			JSONGameArgs.Type type, ClientGame game) {
		JSONObject obj = new JSONObject();
		try {
			obj.put("meta", 		"game");
			obj.put("state", 		state.getValue());
			obj.put("type", 		type.getValue());
			
			JSONObject gameData = new JSONObject();
			gameData.put("playerOne", 	"[]");
			gameData.put("playerTwo", 	"[]");
			gameData.put("playPile", 		new JSONArray(game.getDeck().getPlayPileJSON()));
			gameData.put("discardPile", 	new JSONArray(game.getDeck().getDiscardPileJSON()));
			gameData.put("winningScore", 	game.getPlayer().getDeadwood());
			obj.put("gameData", gameData);
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		
		return obj.toString();
	}
	
	/**
	 * Serializes the start of the game. Quick function to setup the game
	 * @param game	A reference to the clientGame in use
	 * @return	Returns an encoded JSON String of the game
	 */
	public static String serializeGameCreate(ClientGame game) {
		JSONObject obj = new JSONObject();
		
		//- create the players (dealer and non dealer)
		ArrayList<String> playerOne = new ArrayList<String>();
		ArrayList<String> playerTwo = new ArrayList<String>();
		
		//- go through 20 cards, alternate between each card
		for (int i = 0; i < 20; i++) {
			if ((i & 1) == 0) 	{ playerOne.add(game.getDeck().takePlayCard().toString()); }
			else 				{ playerTwo.add(game.getDeck().takePlayCard().toString()); }
		}
		
		//- add one to the discard pile
		game.getDeck().addDiscardCard(game.getDeck().takePlayCard());
		
		//- start adding to a json string
		try {
			obj.put("meta", 		"game");
			obj.put("state", 		JSONGameArgs.State.START.getValue());
			obj.put("type", 		JSONGameArgs.Type.ALL.getValue());
			
			//- add a seperate node for the arrays
			JSONObject gameData 		= new JSONObject();
			gameData.put("playPile", 	new JSONArray(game.getDeck().getPlayPileJSON()));
			gameData.put("discardPile", new JSONArray(game.getDeck().getDiscardPileJSON()));
			gameData.put("playerOne", 	new JSONArray(playerOne));
			gameData.put("playerTwo", 	new JSONArray(playerTwo));
			
			obj.put("gameData", gameData);
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		
		return obj.toString();
	}
	
	/**
	 * Decodes a lobby message and returns it as a collection of JSON lobby arguments
	 * @param json	JSON formatted String that is needed to be parsed
	 * @return A new instance of a JSONLobbyArgs
	 */
	public static JSONLobbyArgs parseLobbyMessage(String json) {
		Log.i(ClientGlobal.LOG_JSON, "Lobby: " + json);
		try {
			JSONLobbyArgs args = new JSONLobbyArgs(new JSONObject(json));
			return args;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	/**
	 * Decodes a game message and returns it as a collection JSON game arguments
	 * @param json	JSON formatted String that is needed to be parsed
	 * @return	A new instance of a JSONGameArgs
	 */
	public static JSONGameArgs parseGameMessage(String json){
		Log.i(ClientGlobal.LOG_JSON, "Game: " + json);
		try {
			JSONGameArgs args = new JSONGameArgs(new JSONObject(json));
			return args;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	/**
	 * Converts a JSONArray to an List of cards
	 * @param obj		The JSONArray
	 * @param array		The Array to implement cards into
	 */
	public static void parseJSONToList(JSONArray obj, List<ClientCard> list) {
		for (int i = 0; i < obj.length(); i++) {
			String[] card;
			try {
				card = obj.get(i).toString().split(";");
				list.add(new ClientCard(
						Integer.parseInt(card[0]),
						Integer.parseInt(card[1]),
						Integer.parseInt(card[2]),
						Integer.parseInt(card[3]),
						Integer.parseInt(card[4])));
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}
}
