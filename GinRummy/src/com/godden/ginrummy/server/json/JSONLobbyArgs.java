package com.godden.ginrummy.server.json;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.godden.ginrummy.client.ClientGlobal;

/**
 * An extension of JSONArgs that is designed to parse lobby specific arguments from the JSONObject
 * @author Joshua Godden
 * @version 1.0
 */
public final class JSONLobbyArgs extends JSONArgs {
	
	/**
	 * Status based enum
	 * @author Joshua Godden
	 */
	public enum Status {
		JOIN(0), START(1), ERROR(2);
		private final int value;
		private Status(final int value) {
			this.value = value;
		}
		public long getValue() {
			return value;
		}
		
		/**
		 * Casts a given Long to an enum of Status
		 * @param x	The Long to be converted
		 * @return	Returns an Enum
		 */
		public static Status cast(int x) {
			if (x == 0) { return Status.JOIN; 	}
			if (x == 1) { return Status.START; 	}
			return Status.ERROR;
		}
	}
	
	/**
	 * The type of status of the lobby
	 */
	public Status status;
	
	/**
	 * Creates and instantiates a new JSON Lobby Arguments
	 * @param json	The JSON to be converted
	 */
	public JSONLobbyArgs(JSONObject json) {
		super(json);
	}

	@Override
	protected void getData() {
		try {
			this.status = Status.cast(json.getInt("status"));
		}
		catch (JSONException e) {
			Log.i(ClientGlobal.LOG_JSON, "JSONLobbyArgs failed to get data");
			e.printStackTrace();
		}
	}

	/**
	 * Gets a reference to the state of the message
	 * @return	Returns the state
	 * @deprecated No need to call getters since variables are public
	 */
	public Status getState() {
		return status;
	}
}
