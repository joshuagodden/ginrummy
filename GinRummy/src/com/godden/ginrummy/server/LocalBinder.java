package com.godden.ginrummy.server;

import android.os.Binder;

/**
 * An extension of Binder to allow an activity to bind to ElectroServer
 * @author Joshua Godden
 * @version 1.0
 */
public class LocalBinder extends Binder {

	private ElectroService _service;
	
	/**
	 * Creates and instantiates a new LocalBinder
	 * @param service	The service to be associated
	 */
	public LocalBinder(ElectroService service) {
		_service = service;
	}
	
	/**
	 * Gets the ElectroServer connection service
	 * @return	A reference to ConnectionService
	 */
	public ElectroService getService() {
		return _service;
	}
}
