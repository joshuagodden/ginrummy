package com.godden.ginrummy;

import android.os.Bundle;
import android.preference.PreferenceFragment;

/**
 * The fragment of the Preferences activity
 * @author Joshua Godden
 * @version 1.0
 */
public class PrefsFragment extends PreferenceFragment {
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.prefs);
	}
}
