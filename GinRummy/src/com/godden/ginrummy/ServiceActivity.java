package com.godden.ginrummy;

import com.godden.ginrummy.client.ClientGlobal;
import com.godden.ginrummy.interfaces.IReceiver;
import com.godden.ginrummy.server.ElectroService;
import com.godden.ginrummy.server.IntentReceiver;
import com.godden.ginrummy.server.LocalBinder;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

/**
 * An extended Activity that handles the binding of the ElectroService and
 * dealing with intents. Has overridable methods to allow customisation for other activities.
 * Even if an Activity does not feature many additional customisations, just override this class to make the binding
 * of ElectroServer far less messier than what is should be.
 * @author Joshua Godden
 */
public class ServiceActivity extends Activity implements IReceiver {
	
	/**
	 * A reference to the ElectroServer service
	 */
	protected ElectroService electroService;
	
	/**
	 * A reference to the IntentReceiver
	 */
	protected IntentReceiver intentReceiver;
	
	/**
	 * A condition based on whether the ElectroServer service is bound to this Activity
	 */
	protected boolean boundService;
	
	/**
	 * A condition based on whether the IntentReceiver has registered to any service events
	 */
	protected boolean intentRegistered;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		boundService 	= false;
		intentReceiver 	= new IntentReceiver(this);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (boundService) {
			electroService.closeConnection();
			unbindService(serviceConnection);
			boundService = false;
		}
		if (intentRegistered) {
			unregisterReceiver(intentReceiver);
			intentRegistered = false;
		}
	}
	
	@Override
	public void receivedBroadcast(Intent intent) {
		if (intent.getAction().equals(ElectroService.NEW_SERVER_MESSAGE)) 	{ handleIntentServer(intent.getExtras()); 	}
		if (intent.getAction().equals(ElectroService.NEW_PUBLIC_MESSAGE)) 	{ handleIntentPublic(intent.getExtras()); 	}
		if (intent.getAction().equals(ElectroService.NEW_LOBBY_MESSAGE))	{ handleIntentLobby(intent.getExtras()); 	}
		if (intent.getAction().equals(ElectroService.NEW_GAME_MESSAGE))		{ handleIntentGame(intent.getExtras()); 	}
	}
	
	/**
	 * Handles the Public intent messages received
	 * @param bundle	The Bundle that is sent via the intent
	 */
	protected void handleIntentPublic(Bundle bundle) {
		Log.i(ClientGlobal.LOG_INFO, "Activity -> Received Intent : Public");
	}
	
	/**
	 * Handles the Server intent messages received
	 * @param bundle	The Bundle that is sent via the intent
	 */
	protected void handleIntentServer(Bundle bundle) {
		Log.i(ClientGlobal.LOG_INFO, "Activity -> Received Intent : Server");
	}
	
	/**
	 * Handles the Lobby intent messages received
	 * @param bundle	The Bundle that is sent via the intent
	 */
	protected void handleIntentLobby(Bundle bundle) {
		Log.i(ClientGlobal.LOG_INFO, "Activity -> Received Intent : Lobby");
	}
	
	/**
	 * Handles the Game intent messages received
	 * @param bundle	The Bundle that is sent via the intent
	 */
	protected void handleIntentGame(Bundle bundle) {
		Log.i(ClientGlobal.LOG_INFO, "Activity -> Received Intent : Game");
	}
	
	/**
	 * Requests a bind to the Service Connection
	 */
	protected void requestBind() {
		bindService(new Intent(ServiceActivity.this, ElectroService.class),
				serviceConnection, Context.BIND_AUTO_CREATE);
		boundService = true;
	}
	
	/**
	 * Registers receivers for the Activity
	 */
	protected void registerReceivers() {
		registerReceiver(intentReceiver, new IntentFilter(ElectroService.NEW_SERVER_MESSAGE));
		intentRegistered = true;
		registerReceiver(intentReceiver, new IntentFilter(ElectroService.NEW_PUBLIC_MESSAGE));
		intentRegistered = true;
		registerReceiver(intentReceiver, new IntentFilter(ElectroService.NEW_LOBBY_MESSAGE));
		intentRegistered = true;
		registerReceiver(intentReceiver, new IntentFilter(ElectroService.NEW_GAME_MESSAGE));
		intentRegistered = true;
	}
	
	/**
	 * An instance of a Service Connection to ElectroServer
	 */
	protected ServiceConnection serviceConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			electroService = ((LocalBinder)service).getService();
			startService(new Intent(ServiceActivity.this, ElectroService.class));
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			electroService = null;
		}
		
	};
	
	/**
	 * Initiailises the bind, be requesting a bind and setting up receivers
	 */
	protected void initialiseBind() {
		Log.i(ClientGlobal.LOG_INFO, "Activity -> Initialise Bind");
		requestBind();
		registerReceivers();
	}
}
