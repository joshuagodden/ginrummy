package com.godden.ginrummy.interfaces;

import android.content.Intent;

/**
 * Any class that is required to recieve broadcasts from a Service should implement this class.
 * @author Joshua Godden
 * @version 1.0
 */
public interface IReceiver {
	void receivedBroadcast(Intent intent);
}
