package com.godden.ginrummy.interfaces;

/**
 * Any class that may contain data that is destroyable should implement this interface to ensure
 * any data is destroyed.
 * @author Joshua Godden
 * @version 1.0
 */
public interface IDestroyable {
	void onDestroy();
}
