package com.godden.ginrummy;

import android.os.Bundle;
import android.preference.PreferenceActivity;

/**
 * The preferences activity for the application
 * @author Joshua Godden
 * @version 1.0
 */
public class PrefsActivity extends PreferenceActivity {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getFragmentManager().beginTransaction()
			.replace(android.R.id.content, new PrefsFragment())
			.commit();
	}
}
