package com.godden.ginrummy.client.views;

import android.content.Context;
import android.graphics.LightingColorFilter;
import android.util.AttributeSet;
import android.widget.Button;

public class GameButton extends Button {

	private int sort;
	private int cardID;
	@SuppressWarnings("unused")
	private int drawableID;
	private boolean selectedGroupRun;
	
	public GameButton(Context context) {
		super(context);
	}

	public GameButton(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public GameButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public void updateCard(int sort, int drawableID, int cardID) {
		this.sort 		= sort;
		this.drawableID = drawableID;
		this.cardID 	= cardID;
		setBackgroundResource(drawableID);
	}
	
	public void enableHighlight() {
		getBackground().setColorFilter(
				new LightingColorFilter(0xff888888, 0x000000));
	}
	
	public void disableHighlight() {
		getBackground().setColorFilter(null);
	}
	
	public int getSortCode() {
		return sort;
	}
	
	public int getCardID() {
		return cardID;
	}
	
	public void toggleGroupRun() {
		selectedGroupRun = !selectedGroupRun;
	}
	
	public void setGroupRun(boolean value) {
		selectedGroupRun = value;
	}
	
	public boolean getGroupRun() {
		return selectedGroupRun;
	}
}
