package com.godden.ginrummy.client.views;

import java.util.ArrayList;

import com.electrotank.electroserver5.client.user.User;
import com.godden.ginrummy.R;
import com.godden.ginrummy.interfaces.IDestroyable;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Represents the layout of the Lobby section of the Game
 * @author Joshua Godden
 */
public class LobbyLayout extends LinearLayout implements IDestroyable {

	protected TextView 	textGameTitle;
	protected TextView 	textConnectionStatus;
	protected EditText 	textGamePlayers;
	protected Button 	buttonLogin;
	protected Button 	buttonStart;
	
	public LobbyLayout(Context context) {
		super(context);
	}
	
	public LobbyLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	public LobbyLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	@Override
	public void onDestroy() {
		textGameTitle 			= null;
		textConnectionStatus 	= null;
		textGamePlayers 		= null;
		buttonLogin 			= null;
		buttonStart 			= null;
	}

	/**
	 * Gets and sets all the views required for the Lobby
	 */
	public void setViews() {
		textGameTitle 			= (TextView)findViewById(R.id.main_lobby_textView_gameTitle);
		textConnectionStatus 	= (TextView)findViewById(R.id.main_lobby_textView_connectionStatus);
		textGamePlayers 		= (EditText)findViewById(R.id.main_lobby_editText_gamePlayers);
		buttonLogin				= (Button)findViewById(R.id.main_lobby_button_login);
		buttonStart				= (Button)findViewById(R.id.main_lobby_button_start);
	}
	
	/**
	 * Sets the Game Players list to the list of users currently available
	 * @param users
	 */
	public void setGamePlayers(ArrayList<User> users) {
		textGamePlayers.getText().clear(); //- clear list (refresh)
		
		//- append players through
		for (byte i = 0; i < users.size(); i++) {
			textGamePlayers.append(users.get(i).getUserName() + "\n");
		}
	}
	
	/**
	 * Disables the entire view so it can longer render, be clickable etc. Useful for when the game is starting
	 * and the login needs to disappear.
	 */
	public void disableAll() {
		
		//- go through all children and disable them
		int childCount = this.getChildCount();
		for (int i = 0; i < childCount; i++) {
			View v = getChildAt(i);
			v.setVisibility(View.GONE);
			v.setEnabled(false);
		}
		
		//- set this layout to gone and disabled
		setVisibility(View.GONE);
		setEnabled(false);
	}
	
	/**
	 * Changes the visibility of the Login button
	 * @param visibility	The visibility type to be set as
	 */
	public void setLoginVisibility(int visibility) {
		buttonLogin.setVisibility(visibility);
	}
	
	/**
	 * Changes the availability of the Login button
	 * @param enabled	Whether its enabled or not
	 */
	public void setLoginEnabled(boolean enabled) {
		buttonLogin.setEnabled(enabled);
	}
	
	/**
	 * Changes the availability of the Start button
	 * @param enabled	Whether its enabled or not
	 */
	public void setStartEnabled(boolean enabled) {
		buttonStart.setEnabled(enabled);
	}
	
	/**
	 * Changes the visibility of the Start Button
	 * @param visibility The visibility type to be set as
	 */
	public void setStartVisibility(int visibility) {
		buttonStart.setVisibility(visibility);
	}
	
	/**
	 * Sets the connection status to Ok
	 */
	public void setConnectionStatusOk() {
		StringBuilder sb = new StringBuilder();
		sb.append(getContext().getString(R.string.main_lobby_textView_connectionStatus));
		sb.append(" ");
		sb.append(getContext().getString(R.string.main_lobby_textView_connectionStatus_ok));
		textConnectionStatus.setText(sb.toString());
	}
	
	/**
	 * Sets the connection status to Fail
	 */
	public void setConnectionStatusFail() {
		StringBuilder sb = new StringBuilder();
		sb.append(getContext().getString(R.string.main_lobby_textView_connectionStatus));
		sb.append(" ");
		sb.append(getContext().getString(R.string.main_lobby_textView_connectionStatus_fail));
		textConnectionStatus.setText(sb.toString());
	}
}
