package com.godden.ginrummy.client.views;

import java.util.ArrayList;

import com.godden.ginrummy.R;
import com.godden.ginrummy.client.ClientCard;
import com.godden.ginrummy.interfaces.IDestroyable;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class GameLayout extends RelativeLayout implements IDestroyable {

	private LinearLayout _cardButtons;
	private Button _playButton;
	private Button _discardButton;
	private Animation _animScale;
	
	public GameLayout(Context context) {
		super(context);
	}

	public GameLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public GameLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	public void setViews() {
		_cardButtons 		= (LinearLayout)findViewById(R.id.main_game_layout_cards);
		_discardButton 		= (Button)findViewById(R.id.main_game_button_discard);
		_playButton 		= (Button)findViewById(R.id.main_game_button_deck);
		_animScale			= AnimationUtils.loadAnimation(this.getContext(), R.anim.anim_scale);
	}
	
	@Override
	public void onDestroy() {
		_cardButtons 	= null;
		_playButton 	= null;
		_discardButton 	= null;
	}
	
	/**
	 * Enables the view of the game
	 */
	public void showAll() {
		setVisibility(View.VISIBLE);
	}
	
	public void hideCards() {
		int childCount = _cardButtons.getChildCount();
		for (int i = 0; i < childCount; i++) {
			View v = _cardButtons.getChildAt(i);
			v.setBackgroundResource(0);
			v.setVisibility(View.GONE);
			v.setEnabled(false);
		}
	}
	
	public void disableCards() {
		int childCount = _cardButtons.getChildCount();
		for (int i = 0; i < childCount; i++) {
			View v = _cardButtons.getChildAt(i);
			v.setEnabled(false);
		}
	}
	
	public void enableCards() {
		int childCount = _cardButtons.getChildCount();
		for (int i = 0; i < childCount; i++) {
			View v = _cardButtons.getChildAt(i);
			v.setEnabled(true);
		}
	}
	
	public GameButton getCard(int index) {
		return (GameButton)_cardButtons.getChildAt(index);
	}
	
	/**
	 * Plays an animation on a card
	 * @param index
	 */
	public void playCardAnimation(int index) {
		final GameButton button = getCard(index);
		button.startAnimation(_animScale);
	}
	
	public void disableDecks() {
		_playButton.setEnabled(false);
		_discardButton.setEnabled(false);
	}
	
	public void enableDecks() {
		_playButton.setEnabled(true);
		_discardButton.setEnabled(true);
	}
	
	public void updatePlayerCards(ArrayList<ClientCard> cards) {
		hideCards(); //- reset
		
		//- go through each player cards, update the view and add the picture
		for (int i = 0; i < cards.size(); i++) {
			GameButton v = (GameButton)_cardButtons.getChildAt(i);
			v.updateCard(cards.get(i).getSort(), cards.get(i).getDrawID(), i);
			v.setVisibility(View.VISIBLE);
			v.setEnabled(true);
			v.setGroupRun(false);
		}
	}
	
	public void updateDiscardCard(ClientCard card) {
		_discardButton.setBackgroundResource(card.getDrawID());
	}
	
	public void hideDiscardCard() {
		_discardButton.setBackgroundResource(0);
		_discardButton.setVisibility(View.VISIBLE);
		_discardButton.setEnabled(false);
	}
}
