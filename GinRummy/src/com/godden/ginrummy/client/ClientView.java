package com.godden.ginrummy.client;

import com.godden.ginrummy.client.views.GameLayout;
import com.godden.ginrummy.client.views.GuiLayout;
import com.godden.ginrummy.client.views.LobbyLayout;
import com.godden.ginrummy.interfaces.IDestroyable;

/**
 * Handles all the graphical views from within the Application.
 * (View)
 * @author Joshua Godden
 * @version 1.0
 */
public final class ClientView implements IDestroyable {

	/**
	 * The graphical layout of the Lobby
	 */
	private LobbyLayout 	lobbyLayout;
	
	/**
	 * The graphical layout of the game
	 */
	private GameLayout 		gameLayout;
	
	/**
	 * The graphical layout of the GUI
	 */
	private GuiLayout		guiLayout;
	
	/**
	 * Creates and instantiates a new ClientView
	 * @param lobbyLayout		The Lobby layout view
	 * @param gameLayout		The Game layout view
	 * @param guiLayout			The GUI layout view
	 */
	public ClientView(LobbyLayout lobbyLayout, GameLayout gameLayout, GuiLayout guiLayout) {
		this.lobbyLayout 	= lobbyLayout;
		this.gameLayout 	= gameLayout;
		this.guiLayout		= guiLayout;
		lobbyLayout.setViews();
		gameLayout.setViews();
		guiLayout.setViews();
	}
	
	/**
	 * Gets the lobby layout
	 * @return	Returns a reference of the LobbyLayout
	 */
	public LobbyLayout getLobby() {
		return lobbyLayout;
	}
	
	/**
	 * Gets the lobby layout
	 * @return	Returns a reference of the GameLayout
	 */
	public GameLayout getGame() {
		return gameLayout;
	}
	
	/**
	 * Gets the lobby layout
	 * @return	Returns a reference of the GuiLayout
	 */
	public GuiLayout getGui() {
		return guiLayout;
	}

	@Override
	public void onDestroy() {
		lobbyLayout.onDestroy();
		gameLayout.onDestroy();
		guiLayout.onDestroy();
	}

}
