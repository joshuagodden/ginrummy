package com.godden.ginrummy.client;

import com.godden.ginrummy.server.json.JSONGameArgs;


/**
 * Holds all of the game data currently available from within the application.
 * (Model)
 * @author Joshua Godden
 */
public final class ClientGame {

	/**
	 * An instance of the player
	 */
	private ClientPlayer 		clientPlayer;
	
	/**
	 * An instance of the decks available
	 */
	private ClientDeck 			clientDeck;
	
	/**
	 * A reference to the current game state
	 */
	private JSONGameArgs.State 	clientState;
	
	/**
	 * Creates and instantiates a new Game
	 */
	public ClientGame() {
		clientPlayer 	= new ClientPlayer();
		clientDeck 		= new ClientDeck();
	}
	
	/**
	 * Creates the game data and sets up the deck
	 */
	public void create() {
		clientDeck.create();
	}
	
	/**
	 * Sets the current state of the game
	 * @param state	The state the game is currently at
	 */
	public void setState(JSONGameArgs.State state) {
		this.clientState = state;
	}
	
	/**
	 * Gets the current state of the game
	 * @return	A value of the state via JSONGameArgs.State
	 */
	public JSONGameArgs.State getState() {
		return clientState;
	}
	
	/**
	 * Gets the player
	 * @return	A reference to the player
	 */
	public ClientPlayer getPlayer() {
		return clientPlayer;
	}

	/**
	 * Gets the deck system
	 * @return A reference to the deck
	 */
	public ClientDeck getDeck() {
		return clientDeck;
	}
}
