package com.godden.ginrummy.client;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Stack;

import org.json.JSONArray;

import com.godden.ginrummy.server.json.JSONComms;

/**
 * Representation of the deck within the game
 * @author Joshua Godden
 * @version 1.0
 */
public final class ClientDeck {
	
	/**
	 * An stack of available to play cards. Using stack for last in, first out
	 */
	private Stack<ClientCard> playPile;
	
	/**
	 * An stack of available discard cards. Using stack for last in, first out
	 */
	private Stack<ClientCard> discardPile;
	
	/**
	 * Creates and instantiates a new ClientDeck
	 */
	public ClientDeck() {
		playPile 	= new Stack<ClientCard>();
		discardPile = new Stack<ClientCard>();
	}
	
	/**
	 * Creates and sets up the initial shuffled deck. This is required to start to the gameplay
	 * and allow the JSON information to be passed.
	 */
	public void create() {
		
		//- get our global setup, and start adding to the play pile, which needs to be shuffled
		for (int i = 0; i < ClientGlobal.GAME_CARDS.length; i++) {
			final String[] card = ((String)ClientGlobal.GAME_CARDS[i]).split(";");
			
			//- parse the card
			playPile.add(new ClientCard(
					Integer.parseInt(card[0]),
					Integer.parseInt(card[1]),
					Integer.parseInt(card[2]),
					Integer.parseInt(card[3]),
					Integer.parseInt(card[4])));
		}
		
		//- when its all done, shuffle with the Collections method
		Collections.shuffle(playPile);
	}
	
	/**
	 * Adds a card to the play pile
	 * @param card The card to add
	 */
	public void addPlayCard(ClientCard card) {
		playPile.add(card);
	}
	
	/**
	 * Adds a card to the discard pile
	 * @param card	The card to add
	 */
	public void addDiscardCard(ClientCard card) {
		discardPile.add(card);
	}
	
	/**
	 * Removes a card from the play pile and returns it
	 * @return	Returns a play card
	 */
	public ClientCard takePlayCard() {
		return playPile.pop();
	}
	
	/**
	 * Removes a card from the discard pile and returns it
	 * @return	Returns a discard card
	 */
	public ClientCard takeDiscardCard() {
		return discardPile.pop();
	}
	
	/**
	 * Assigns a JSONArray play pile to the play deck. 
	 * @param obj	The JSONArray that is required to be parsed
	 */
	public void assignPlayPile(JSONArray obj) {
		playPile.clear();
		JSONComms.parseJSONToList(obj, playPile);
	}
	
	/**
	 * Assigns a JSONArray discard pile to the discard deck. 
	 * @param obj	The JSONArray that is required to be parsed
	 */
	public void assignDiscardPile(JSONArray obj) {
		discardPile.clear();
		JSONComms.parseJSONToList(obj, discardPile);
	}
	
	/**
	 * Gets the play pile
	 * @return Returns a Stack with the playable cards
	 */
	public Stack<ClientCard> getPlayPile() {
		return playPile;
	}
	
	/**
	 * Gets the discard pile
	 * @return Returns a Stack with the discardable cards
	 */
	public Stack<ClientCard> getDiscardPile() {
		return discardPile;
	}
	
	/**
	 * Gets the play pile with string values of each card for JSON encoding
	 * @return	Returns an ArrayList of Strings representing each card
	 */
	public ArrayList<String> getPlayPileJSON() {
		ArrayList<String> pile = new ArrayList<String>();
		for (int i = 0; i < playPile.size(); i++) {
			pile.add(playPile.get(i).toString());
		}
		return pile;
	}
	
	/**
	 * Gets the discard pile with string values of each card for JSON encoding
	 * @return	Returns an ArrayList of Strings representing each card
	 */
	public ArrayList<String> getDiscardPileJSON() {
		ArrayList<String> pile = new ArrayList<String>();
		for (int i = 0; i < discardPile.size(); i++) {
			pile.add(discardPile.get(i).toString());
		}
		return pile;
	}
}
