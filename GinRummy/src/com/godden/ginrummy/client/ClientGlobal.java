package com.godden.ginrummy.client;

import com.godden.ginrummy.R;

/**
 * A static class holding any needed Global variables along with debugging and translations
 * @author Joshua Godden
 */
public final class ClientGlobal {
	
	/**
	 * Logs to LogCat through tag LOG (Generic outputs)
	 */
	public static final String LOG_INFO 		= "LogInfo";
	
	/**
	 * Logs to LogCat through tag ERROR (Major issues that can cause breaks)
	 */
	public static final String LOG_ERROR 		= "LogError";
	
	/**
	 * Logs to LogCat through tag WARNING (Warnings but not errors that cause breaks)
	 */
	public static final String LOG_WARNING 		= "LogWarning";
	
	/**
	 * Logs to LogCat through tag SERVICE (ElectroServer details)
	 */
	public static final String LOG_SERVICE		= "LogService";
	
	/**
	 * Logs to LogCat through tag GAMEPLAY (Gameplay details)
	 */
	public static final String LOG_GAMEPLAY		= "LogGameplay";
	
	/**
	 * Logs to LogCat through tag JSON (JSON messages, try to keep seperate from ElectroServer)
	 */
	public static final String LOG_JSON			= "LogJSON";
	
	/**
	 * Stores the deadwood score
	 */
	public static final int GAME_DEADWOOD_SCORE	= 10;
	
	/**
	 * Stores the winning score
	 */
	public static final int GAME_WINNING_SCORE = 25;
	
	/**
	 * Stores all the suits
	 */
	public static final int[] GAME_CARDS_SUITS 	= {
		0,
		1,
		2,
		3
	};
	
	/**
	 * Stores all the pips
	 */
	public static final int[] GAME_CARDS_PIPS 	= {
		1,
		2,
		3,
		4,
		5,
		6,
		7,
		8,
		9,
		10,
		11,
		12,
		13
	};
	
	/**
	 * An Array which holds information about all Cards in play
	 * A String based Array format, which is written as:
	 * "C;A;2130837601;1;0"
	 * 
	 * ;			-> Delim
	 * 0,1,2,3 		-> Card suit
	 * 1,2,3,4...	-> Card pip
	 * 2130836701 	-> Drawable ID (R.drawable.card_clubs_a)
	 * 1			-> Points
	 * 0 			-> Sorting index
	 */
	public static final String[] GAME_CARDS = {
		"0;1;" 	+ R.drawable.card_clubs_a 		+ ";1;0",
		"0;2;" 	+ R.drawable.card_clubs_2 		+ ";2;1",
		"0;3;" 	+ R.drawable.card_clubs_3 		+ ";3;2",
		"0;4;" 	+ R.drawable.card_clubs_4 		+ ";4;3",
		"0;5;" 	+ R.drawable.card_clubs_5 		+ ";5;4",
		"0;6;" 	+ R.drawable.card_clubs_6 		+ ";6;5",
		"0;7;" 	+ R.drawable.card_clubs_7 		+ ";7;6",
		"0;8;" 	+ R.drawable.card_clubs_8 		+ ";8;7",
		"0;9;" 	+ R.drawable.card_clubs_9 		+ ";9;8",
		"0;10;" + R.drawable.card_clubs_10 		+ ";10;9",
		"0;11;" + R.drawable.card_clubs_j 		+ ";10;10",
		"0;12;" + R.drawable.card_clubs_q 		+ ";10;11",
		"0;13;" + R.drawable.card_clubs_k 		+ ";10;12",
		"1;1;"	+ R.drawable.card_diamonds_a 	+ ";1;13",
		"1;2;"	+ R.drawable.card_diamonds_2 	+ ";2;14",
		"1;3;" 	+ R.drawable.card_diamonds_3 	+ ";3;15",
		"1;4;" 	+ R.drawable.card_diamonds_4 	+ ";4;16",
		"1;5;" 	+ R.drawable.card_diamonds_5 	+ ";5;17",
		"1;6;" 	+ R.drawable.card_diamonds_6 	+ ";6;18",
		"1;7;" 	+ R.drawable.card_diamonds_7	+ ";7;19",
		"1;8;" 	+ R.drawable.card_diamonds_8 	+ ";8;20",
		"1;9;" 	+ R.drawable.card_diamonds_9 	+ ";9;21",
		"1;10;" + R.drawable.card_diamonds_10 	+ ";10;22",
		"1;11;" + R.drawable.card_diamonds_j 	+ ";10;23",
		"1;12;" + R.drawable.card_diamonds_q 	+ ";10;24",
		"1;13;" + R.drawable.card_diamonds_k 	+ ";10;25",
		"2;1;" 	+ R.drawable.card_hearts_a 		+ ";1;26",
		"2;2;" 	+ R.drawable.card_hearts_2 		+ ";2;27",
		"2;3;" 	+ R.drawable.card_hearts_3 		+ ";3;28",
		"2;4;" 	+ R.drawable.card_hearts_4 		+ ";4;29",
		"2;5;" 	+ R.drawable.card_hearts_5 		+ ";5;30",
		"2;6;" 	+ R.drawable.card_hearts_6 		+ ";6;31",
		"2;7;" 	+ R.drawable.card_hearts_7 		+ ";7;32",
		"2;8;" 	+ R.drawable.card_hearts_8 		+ ";8;33",
		"2;9;" 	+ R.drawable.card_hearts_9 		+ ";9;34",
		"2;10;" + R.drawable.card_hearts_10 	+ ";10;35",
		"2;11;" + R.drawable.card_hearts_j 		+ ";10;36",
		"2;12;" + R.drawable.card_hearts_q 		+ ";10;37",
		"2;13;" + R.drawable.card_hearts_k 		+ ";10;38",
		"3;1;" 	+ R.drawable.card_spades_a 		+ ";1;39",
		"3;2;" 	+ R.drawable.card_spades_2 		+ ";2;40",
		"3;3;" 	+ R.drawable.card_spades_3 		+ ";3;41",
		"3;4;" 	+ R.drawable.card_spades_4 		+ ";4;42",
		"3;5;" 	+ R.drawable.card_spades_5 		+ ";5;43",
		"3;6;" 	+ R.drawable.card_spades_6 		+ ";6;44",
		"3;7;" 	+ R.drawable.card_spades_7 		+ ";7;45",
		"3;8;" 	+ R.drawable.card_spades_8 		+ ";8;46",
		"3;9;" 	+ R.drawable.card_spades_9 		+ ";9;47",
		"3;10;" + R.drawable.card_spades_10 	+ ";10;48",
		"3;11;" + R.drawable.card_spades_j 		+ ";10;49",
		"3;12;" + R.drawable.card_spades_q 		+ ";10;50",
		"3;13;" + R.drawable.card_spades_k 		+ ";10;51"
	};
	
	/**
	 * Gets the suit of the card and returns it as a String
	 * @param suit	The suit to check
	 * @return	Returns a String with the translated Suit
	 */
	public static final String getSuit(int suit)
	{
		switch (suit) {
			case 0	: return "C";
			case 1	: return "D";
			case 2	: return "H";
			case 3	: return "S";
			default	: return "ERROR";
		}
	}
	
	/**
	 * Gets the pip of the card and returns it as a String
	 * @param pip	The pip to check
	 * @return	Returns a String with the translated pip
	 */
	public static final String getPip(int pip)
	{
		switch (pip) {
			case 1	:	return "A";
			case 2	:	return "2";
			case 3	:	return "3";
			case 4	:	return "4";
			case 5	:	return "5";
			case 6	:	return "6";
			case 7	:	return "7";
			case 8	:	return "8";
			case 9	:	return "9";
			case 10	:	return "10";
			case 11	:	return "J";
			case 12	:	return "Q";
			case 13 :	return "K";
			default	:	return "ERROR";
		}
	}
}
