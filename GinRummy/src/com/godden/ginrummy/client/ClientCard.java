package com.godden.ginrummy.client;

/**
 * A representation of a Card
 * @author Joshua Godden
 * @version 1.0
 */
public final class ClientCard implements Comparable<ClientCard> {

	/**
	 * Suit of the card
	 */
	private int suit;
	
	/**
	 * Pip/value of the card
	 */
	private int pip;
	
	/**
	 * Drawable ID of the card
	 */
	private int drawID;
	
	/**
	 * The points of the card
	 */
	private int points;
	
	/**
	 * The sort code of the card
	 */
	private int sort;
	
	/**
	 * The melded status of the card
	 */
	private boolean melded;
	
	/**
	 * Creates and instantiates a new Card
	 * @param suit		The Suit value of the card
	 * @param pip		The Pip (number) value of the card
	 * @param drawID	The DrawableID of the card
	 * @param points	The amount of points this card holds
	 * @param sort		The sort code this card holds (for Collection.sort)
	 */
	public ClientCard(int suit, int pip, int drawID, int points, int sort) {
		this.suit 	= suit;
		this.pip 	= pip;
		this.drawID = drawID;
		this.points = points;
		this.sort 	= sort;
		this.melded	= false;
	}

	/**
	 * Gets the card's suit
	 * @return	Returns an int of the suit
	 */
	public int getSuit() {
		return suit;
	}
	
	/**
	 * Gets the card's pip
	 * @return	Returns an int of the pip
	 */
	public int getPip() {
		return pip;
	}
	
	/**
	 * Gets the card's points
	 * @return	Returns an int of the points
	 */
	public int getPoints() {
		return points;
	}
	
	/**
	 * Gets the card's sort code
	 * @return	Returns an int of the sort code
	 */
	public int getSort() {
		return sort;
	}
	
	/**
	 * Gets the card's draw ID
	 * @return	Returns an int of the draw ID
	 */
	public int getDrawID() {
		return drawID;
	}
	
	/**
	 * Gets the card's melded status
	 * @return	Returns an boolean of the melded status
	 */
	public boolean getMelded() {
		return melded;
	}
	
	/**
	 * Sets the card's melded status
	 * @param value	The boolean value
	 */
	public void setMelded(boolean value) {
		this.melded = value;
	}
	
	/**
	 * Prints the card out as a readable String
	 * @return	Returns a String of the printed/translated card
	 */
	public String printCard() {
		return ClientGlobal.getSuit(suit) + ":" + ClientGlobal.getPip(pip);
	}
	
	@Override
	public String toString() {
		return suit + ";" + pip + ";" + drawID + ";" + points + ";" + sort;
	}


	@Override
	public int compareTo(ClientCard another) {
		return (sort - another.getSort());
	}
}
