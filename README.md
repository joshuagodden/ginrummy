# README #

A Java developed multiplayer Ginrummy game using the Android Development Framework as the client and ElectroServer for the server.

This application developed and progressed relatively well but due to ElectroServer's incompatibilities with JRE 1.7, a majority of the functionality
could no longer work. However the messaging functions still work correctly.

Instead, the game became an authoritative client and messages were exchanged through a variety of JSON encoded strings. I specifically coded a JSON
encoder and decoder to handle the game messages using the already provided Java JSON parsers.

Some of the code is somewhat unclean and some functions are not correctly commented. This is due to the lack of time available and the continuously changing
specification required by the module due to the faults occurring with ElectroServer.

# What could be improved #
* Better state handling! BY FARRRRRRRR!
* Main class is a mess!
* Goes to show what rushing can do to programming!