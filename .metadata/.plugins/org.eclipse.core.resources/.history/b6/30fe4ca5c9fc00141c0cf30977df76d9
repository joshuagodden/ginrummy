package com.godden.ginrummy.client;

/**
 * A representation of a Card
 * @author Joshua Godden
 */
public final class ClientCard implements Comparable<ClientCard> {

	protected int suit;
	protected int pip;
	protected int drawID;
	protected int points;
	protected int sort;
	
	/**
	 * Creates and instantiates a new Card
	 * @param suit		The Suit value of the card
	 * @param pip		The Pip (number) value of the card
	 * @param drawID	The DrawableID of the card
	 * @param points	The amount of points this card holds
	 * @param sort		The sort code this card holds (for Collection.sort)
	 */
	public ClientCard(int suit, int pip, int drawID, int points, int sort) {
		this.suit 	= suit;
		this.pip 	= pip;
		this.drawID = drawID;
		this.points = points;
		this.sort 	= sort;
	}

	
	public int getSuit() {
		return suit;
	}
	
	public int getPip() {
		return pip;
	}
	
	public int getPoints() {
		return points;
	}
	
	public int getSortCode() {
		return sort;
	}
	
	@Override
	public String toString() {
		return suit + ";" + pip + ";" + drawID + ";" + points + ";" + sort;
	}


	@Override
	public int compareTo(ClientCard another) {
		return (sort - another.getSortCode()());
	}
}
