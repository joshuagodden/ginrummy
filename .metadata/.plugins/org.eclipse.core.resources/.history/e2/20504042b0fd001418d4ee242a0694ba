package com.godden.ginrummy.client;

import java.util.ArrayList;
import java.util.Collections;

import org.json.JSONArray;

import android.util.Log;

import com.godden.ginrummy.interfaces.IDestroyable;
import com.godden.ginrummy.server.json.JSONComms;

/**
 * Represents a player in game
 * @author Joshua Godden
 */
public final class ClientPlayer implements IDestroyable {

	protected boolean playing;
	protected String username;
	protected boolean knocked;
	
	protected ArrayList<ClientCard> cards;
	protected ArrayList<ArrayList<ClientCard>> groups;
	protected ArrayList<ArrayList<ClientCard>> runs;
	
	protected int deadwood;
	protected int score;
	
	/**
	 * Creates and instantiates a new Player
	 */
	public ClientPlayer() {
		playing 	= false;
		knocked		= false;
		cards 		= new ArrayList<ClientCard>();
		groups		= new ArrayList<ArrayList<ClientCard>>();
		runs		= new ArrayList<ArrayList<ClientCard>>();
	}
	
	public void calculateDeadwood() {
		deadwood = 0;
		for (int i = 0; i < cards.size(); i++) {
			ClientCard card = cards.get(i);
			
			//- check this card hasn't been melded
			if (!card.getMelded()) {
				deadwood += cards.get(i).getPoints();
			}
		}
	}
	
	private void resetMelds() {
		for (int i = 0; i < cards.size(); i++) {
			cards.get(i).setMelded(false);
		}
	}
	
	public void addCard(ClientCard card) {
		cards.add(card);
	}
	
	public ClientCard removeCard(int sortCode) {
		int index = -1;
		for (int i = 0; i < cards.size(); i++) {
			if (cards.get(i).getSort() == sortCode) {
				index = i;
				break;
			}
		}
		return cards.remove(index);
	}
	
	/**
	 * Sets the cards
	 * @param obj	JSON Array
	 */
	public void assignCards(JSONArray obj) {
		
		//- clear the cards, convert json to array and sort
		cards.clear();
		JSONComms.convertJSONToList(obj, cards);
		Collections.sort(cards);
		
		Log.i(ClientGlobal.LOG_GAMEPLAY, username + " has cards: " + cards.toString());
	}
	
	public void setPlaying(boolean value) {
		playing = value;
	}
	
	public void togglePlaying() {
		playing = !playing;
	}
	
	/**
	 * Gets all the group
	 * @return	Return an Array of Groups
	 */
	public void findGroups() {
		
		//- clear out the groups
		groups.clear();
		resetMelds();
		
		for (int x = 0; x < ClientGlobal.GAME_CARDS_PIPS.length; x++) {
			int[] indices 	= new int[4]; //- can only have 4 indexes max for a group
			int counter 	= 0;
			
			for (int y = 0; y < cards.size(); y++) {
				
				//- does the card pip match the game cards?
				if (cards.get(y).getPip() == ClientGlobal.GAME_CARDS_PIPS[x]) {
					indices[counter] = y;
					counter++;
				}
			}
			
			//- hit over the counter, and reached the end of the cards? add the new group
			if (counter >= 3) {
				ArrayList<ClientCard> group = new ArrayList<ClientCard>();
				
				//- go through the found indices
				for (int z = 0; z < counter; z++) {
					ClientCard card = cards.get(indices[z]);
					card.setMelded(true);
					group.add(card);
				}
				
				groups.add(group);
				counter = 0;
			}
		}
		
		Log.i(ClientGlobal.LOG_GAMEPLAY, getUsername() + " has " + groups.size() + " groups");
	}
	
	/**
	 * SLOW METHOD
	 * Finds all the runs from within the players cards.
	 * @return	Return an Array of Runs
	 */
	public void findRuns() {
		
		//- clear out the runs
		runs.clear();
		
		for (int x = 0; x < ClientGlobal.GAME_CARDS_SUITS.length; x++) {
			int[] indices 	= new int[11];
			int counter 	= 0;
			
			//- go through player cards, check not melded and add its pos
			for (int y = 0; y < cards.size(); y++) {
				if (!cards.get(y).getMelded()) {
					if (cards.get(y).getSuit() == ClientGlobal.GAME_CARDS_SUITS[x]) {
						indices[counter] = y;
						counter++;
					}
				}
			}
			
			//- counter is over 3? loop through found suits and check if they work
			if (counter >= 3) {
				ArrayList<ClientCard> run = new ArrayList<ClientCard>();
				for (int z = 0; z < counter; z++) {
					if (z <= (counter - 1)) {
						Log.i(ClientGlobal.LOG_GAMEPLAY, Integer.toString((cards.get(indices[z]).getSort() + 1)));
						Log.i(ClientGlobal.LOG_GAMEPLAY, Integer.toString((cards.get(indices[z + 1]).getSort())));
						
						//- add the first one
						cards.get(indices[z]).setMelded(true);
						run.add(cards.get(indices[z]));
						
						//- if the next card is playable, continue it on
						if (((cards.get(indices[z]).getSort()) + 1) ==
								(cards.get(indices[z + 1]).getSort())) {
							continue;
						}
						else {
							cards.
							run.clear();
						}
						
						//- check the next in advance
						/*
						if (((cards.get(indices[z]).getSort()) + 1) ==
								(cards.get(indices[z + 1]).getSort())) {
							cards.get(indices[z]).setMelded(true);
							run.add(cards.get(indices[z]));
							Log.i(ClientGlobal.LOG_GAMEPLAY, "Run possible: " + run.size());
						}
						else {
							Log.i(ClientGlobal.LOG_GAMEPLAY, "Not possible");
						}*/
					}
				}
				
				//- is the run size bigger than 3? if so add it to the runs else reset everything
				if (run.size() >= 3) {
					runs.add(run);
				}
				else {
					for (int q = 0; q < counter; q++) {
						cards.get(indices[q]).setMelded(false);
					}
				}
			}
		}
		
		Log.i(ClientGlobal.LOG_GAMEPLAY, getUsername() + " has " + runs.size() + " runs");
		
		//- go through every suit
		/*
		for (int x = 0; x < ClientGlobal.GAME_CARDS_SUITS.length; x++) {
			
			int[] indices 	= new int[11]; //- max 11 in a run
			int counter 	= 0;
			
			//- go through each player card
			for (int y = 0; y < cards.size(); y++) {
				
				if (cards.get(y).getSuit() == ClientGlobal.GAME_CARDS_SUITS[x]) {
					if (!cards.get(y).getMelded()) {
						indices[counter] = y;
						counter++;
					}
				}
			}
			
			//- hit over the counter, check the run
			if (counter >= 3) {
				ArrayList<ClientCard> run = new ArrayList<ClientCard>();
				
				for (int z = 0; z < counter; z++) {
					ClientCard card = cards.get(indices[z]);
					
					if (z != (counter -1)) {
						if ((card.getSort() + 1) == (cards.get(indices[z + 1]).getSort())) {
							card.setMelded(true);
							run.add(card);
						}
					}
				}
				
				//- if there is more than 3 cards for a run add it to the runs, else reset the previous lot
				
				if (runs.size() >= 3) { 
					runs.add(run);
				}
				else {
					for (int k = 0; k < counter; k++) {
						cards.get(indices[k]).setMelded(false);
					}
				}
				counter = 0;
			}
		}*/
	}
	
	public ArrayList<ArrayList<ClientCard>> getGroups() {
		return groups;
	}
	
	public ArrayList<ArrayList<ClientCard>> getRuns() {
		return runs;
	}
	
	public void sortCards() {
		Collections.sort(cards);
	}
	
	/**
	 * Sets the players username
	 * @param username The username of the player
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getUsername() {
		return username;
	}
	
	public boolean getPlaying() {
		return playing;
	}
	
	public int getDeadwood() {
		return deadwood;
	}
	
	public String getMelds() {
		StringBuilder b = new StringBuilder();
		for (int x = 0; x < groups.size(); x++) {
			ArrayList<ClientCard> group = groups.get(x);
			for (int y = 0; y < group.size(); y++) {
				if (y != 0) { b.append(","); }
				b.append(group.get(y).printCard());
			}
			
			b.append(";");
		}
		b.append(" - ");
		for (int n = 0; n < runs.size(); n++) {
			ArrayList<ClientCard> run = runs.get(n);
			for (int m = 0; m < run.size(); m++) {
				if (m != 0) { b.append(","); }
				b.append(run.get(m).printCard());
			}
			
			b.append(";");
		}
		return b.toString();
	}
	
	public int getScore() {
		return score;
	}
	
	public boolean getKnocked() {
		return knocked;
	}
	
	public void setKnocked(boolean value) {
		this.knocked = value;
	}
	
	/**
	 * Gets all the cards and returns it as a JSON file
	 * @return	
	 */
	public ArrayList<String> getCardsJSON() {
		ArrayList<String> cp = new ArrayList<String>();
		for (int i = 0; i < cards.size(); i++) {
			cp.add(cards.get(i).toString());
		}
		return cp;
	}
	
	public ArrayList<ClientCard> getCards() {
		return cards;
	}

	@Override
	public void onDestroy() {
		cards.clear();
		groups.clear();
		runs.clear();
	}
}
