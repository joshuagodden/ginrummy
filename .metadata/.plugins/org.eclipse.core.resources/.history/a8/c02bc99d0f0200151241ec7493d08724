package com.godden.ginrummy.server.json;

import org.json.JSONObject;

/**
 * A abstract class used for creating argument set ups for JSON parsing.
 * @author Joshua Godden
 */
public abstract class JSONArgs {

	/**
	 * A reference to the JSONObject that has been passed
	 */
	protected JSONObject json;
	
	/**
	 * Creates and instantiates a new JSONArgs
	 * @param json The JSONObject that was parsed
	 */
	public JSONArgs(JSONObject json) {
		this.json = json;
		getData();
	}
	
	/**
	 * Gets the raw JSONObject
	 * @return	A JSONObject
	 */
	public JSONObject getJSON() {
		return json;
	}
	
	/**
	 * Gets the data from the JSONObject. Should be your own implementation
	 */
	protected abstract void getData();
}
