package com.godden.ginrummy;

import java.util.ArrayList;

import com.electrotank.electroserver5.client.user.User;
import com.godden.ginrummy.client.ClientGame;
import com.godden.ginrummy.client.ClientGlobal;
import com.godden.ginrummy.client.ClientView;
import com.godden.ginrummy.client.views.GameButton;
import com.godden.ginrummy.client.views.GameLayout;
import com.godden.ginrummy.client.views.LobbyLayout;
import com.godden.ginrummy.client.views.GuiLayout;
import com.godden.ginrummy.server.json.JSONComms;
import com.godden.ginrummy.server.json.JSONGameArgs;
import com.godden.ginrummy.server.json.JSONLobbyArgs;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

/**
 * The main game activity
 * @author Joshua Godden
 */
public class GameActivity extends ServiceActivity {
	
	/**
	 * The View
	 */
	protected ClientView clientView;
	
	/**
	 * The Model
	 */
	protected ClientGame clientGame;
	
	/**
	 * A reference to the clients preferences
	 */
	protected SharedPreferences preferences;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		//- setup preferences
		preferences = PreferenceManager.getDefaultSharedPreferences(this);
		
		//- setup clientView
		clientView = new ClientView(
				(LobbyLayout)findViewById(R.id.main_lobby_layout),
				(GameLayout)findViewById(R.id.main_game_layout),
				(GuiLayout)findViewById(R.id.main_gui_layout));
		
		//- setup game
		clientGame = new ClientGame();
		clientGame.getPlayer().setUsername(getPreferenceUsername());
		
		//- initialises the bind
		initialiseBind();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		Log.i(ClientGlobal.LOG_INFO, "On Resume");
		
		//- check username hasn't updated
		final String cu = clientGame.getPlayer().getUsername();
		final String nu = getPreferenceUsername();
		if (!cu.equals(nu)) {
			clientGame.getPlayer().setUsername(nu);
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.lobby_actions, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.lobby_actions_settings:
				startActivity(new Intent(this, PrefsActivity.class));
				break;
			default:
				break;
		}
		return true;
	}
	
	/**
	 * Gets the preferences username
	 * @return
	 */
	protected String getPreferenceUsername() {
		String userkey = getString(R.string.prefs_user_username_key);
		return preferences.getString(userkey, "");
	}
	
	@Override
	protected void handleIntentServer(Bundle bundle) {
		super.handleIntentServer(bundle);
		final String type = bundle.getString("type");
		
		//- electroserver : connected succesfully
		if (type.equals("connected")) {
			clientView.getLobby().setLoginEnabled(true);
			clientView.getLobby().setConnectionStatusOk();
		}
		
		//- electroserver : connected failure
		if (type.equals("disconnected")) {
			clientView.getLobby().setConnectionStatusFail();
		}
		
		//- electroserver : user logged in
		if (type.equals("login")) {
			electroService.requestCreateRoom();
		}
		
		//- electroserver : user joined a room
		if (type.equals("join")) {
			clientView.getLobby().setLoginEnabled(false);
			electroService.requestLobbyMessage(JSONComms.serializeLobby(
					JSONLobbyArgs.Status.JOIN));
		}
		
		//- electroserver : error thrown
		if (type.equals("error")) {
			clientView.getLobby().setLoginEnabled(false);
			clientView.getLobby().setStartEnabled(false);
		}
	}
	
	@Override
	protected void handleIntentLobby(Bundle bundle) {
		super.handleIntentLobby(bundle);
		
		//- get the lobby arguments
		final JSONLobbyArgs args = JSONComms.decodeLobbyMessage(
				bundle.getString("message"));
		
		//- lobby state == join : player has joined the lobby
		if (args.getState() == JSONLobbyArgs.Status.JOIN) {
			
			//- check its the first player
			ArrayList<User> users = electroService.getUsers();
			
			//- if the group size is now 1, the first player is now the dealer
			if (users.size() == 1) {
				clientGame.getPlayer().togglePlaying();
				Log.i(ClientGlobal.LOG_GAMEPLAY, clientGame.getPlayer().getUsername() + " is playing");
			}
			
			//- update the lobby view
			clientView.getLobby().setStartEnabled(true);
			clientView.getLobby().setGamePlayers(electroService.getUsers());
		}
		
		//- lobby state == start : player has started the game
		if (args.getState() == JSONLobbyArgs.Status.START) {
			clientView.getLobby().disableAll();
			clientView.getGame().showAll();
			
			//- if the player is a dealer, start out the game
			if (clientGame.getPlayer().getPlaying()) {
				electroService.requestGameMessage(JSONComms.serializeGame(JSONGameArgs.State.CREATE,
						JSONGameArgs.Type.ALL, clientGame));
			}
		}
	}
	
	@Override
	protected void handleIntentGame(Bundle bundle) {
		super.handleIntentGame(bundle);
		
		//- get our JSONGameArgs, and set the state
		final JSONGameArgs args = JSONComms.decodeGameMessage(
				bundle.getString("message"));
		clientGame.setState(args.getState());
		
		//- bit of debugging
		Log.i(ClientGlobal.LOG_GAMEPLAY, "Game is at: " + args.getState());
		Log.i(ClientGlobal.LOG_GAMEPLAY, clientGame.getPlayer().getUsername() + " is playing? " + clientGame.getPlayer().getPlaying());
		
		//=============================================== INSTANTIATE =========================================
		//- game state == create : Setup the game
		if (args.getState() == JSONGameArgs.State.CREATE) {
			
			//- show gui
			clientView.getGui().setVisibility(View.VISIBLE);
			
			//- is it a dealer? Setup the cards, else show the waiting screen
			if (clientGame.getPlayer().getPlaying()) {
				clientGame.create();
				electroService.requestGameMessage(JSONComms.serializeGameCreate(clientGame));
			}
		}
		
		//- game state == start : Start the game
		if (args.getState() == JSONGameArgs.State.START) {
			
			//- dealer is player one, and player two gets the other
			if (clientGame.getPlayer().getPlaying()) {
				clientGame.getPlayer().assignCards(args.playerOne);
			}
			else {
				clientGame.getPlayer().assignCards(args.playerTwo);
			}
			
			//- updates the state
			handleStateControl(args);
			
			//- if its the player, then send out the start message
			if (clientGame.getPlayer().getPlaying()) {
				electroService.requestGameMessage(JSONComms.serializeGame(
						JSONGameArgs.State.TAKE,
						JSONGameArgs.Type.ALL,
						clientGame));
			}
		}
		//=====================================================================================================
		
		//=============================================== GAMEPLAY ============================================
		//- game state == take : Player is taking a card
		if (args.getState() == JSONGameArgs.State.TAKE) {
			
			//- calculate each others deadwood
			handleStateControl(args);
			
			//- disable the player from touching the decks
			if (!clientGame.getPlayer().getPlaying()) {
				clientView.getGame().disableDecks();
				clientView.getGame().disableCards();
				clientView.getGui().displayMessage(R.string.main_game_layout_messages_state_wait);
			}
			else {
				clientView.getGame().enableDecks();
				clientView.getGame().enableCards();
				clientView.getGui().displayMessage(R.string.main_game_layout_messages_state_take);
			}
		}
		
		//- game state == discard : Player is discarding a card
		if (args.getState() == JSONGameArgs.State.DISCARD) {
			
			//- sort the cards
			clientGame.getPlayer().sortCards();
			
			//- update state
			handleStateControl(args);
			
			//- check its the current
			if (clientGame.getPlayer().getPlaying()) {
				
				//- game ends
				if (clientGame.getDeck().getPlayPile().size() <= 2) {
					electroService.requestGameMessage(JSONComms.serializeGame(
							JSONGameArgs.State.DRAW,
							JSONGameArgs.Type.ALL,
							clientGame));
					return;
				}
				
				//- carry on
				clientView.getGui().displayMessage(R.string.main_game_layout_messages_state_discard);
			}
		}
		
		//- game state == end : Player has finished everything, time to switch
		if (args.getState() == JSONGameArgs.State.END) {
			
			//- sort the players cards
			clientGame.getPlayer().sortCards();
			
			//- update control
			handleStateControl(args);
			
			//- switch the players
			clientGame.getPlayer().togglePlaying();
			
			//- check its current - send out START request
			if (clientGame.getPlayer().getPlaying()) {
				electroService.requestGameMessage(JSONComms.serializeGame(
						JSONGameArgs.State.TAKE,
						JSONGameArgs.Type.ALL,
						clientGame));
			}
		}
		
		//- game state == finish : Game has ended because of multiple reasons
		if (args.getState() == JSONGameArgs.State.FINISH) {
			
			//- disable all
			clientView.getGame().disableDecks();
			clientView.getGame().disableCards();
		}
		
		if (args.getState() == JSONGameArgs.State.DRAW) {
			
			//- disable all
			clientView.getGame().disableDecks();
			clientView.getGame().disableCards();
			
			
		}
		//=====================================================================================================
	}
	
	@Override
	protected void handleIntentPublic(Bundle bundle) {
		super.handleIntentPublic(bundle);
	}
	
	/**
	 * Updates the game
	 * @param args
	 */
	protected void handleStateControl(JSONGameArgs args) {
		
		//- work out the groups & runs (must come beforehand)
		clientGame.getPlayer().findGroups();
		clientGame.getPlayer().findRuns();
		clientView.getGui().updateMelds(clientGame.getPlayer().getMelds());
		
		//- update deadwood
		clientGame.getPlayer().calculateDeadwood();
		clientView.getGui().updateDeadwood(clientGame.getPlayer().getDeadwood());
		
		//- update internals
		clientGame.getDeck().assignPlayPile(args.playPile);
		clientGame.getDeck().assignDiscardPile(args.discardPile);
		
		//- update the view, we are all good to go
		clientView.getGame().updatePlayerCards(clientGame.getPlayer().getCards());
		
		//- check there is actually cards in the discard pile
		if (clientGame.getDeck().getDiscardPile().size() >= 1) {
			clientView.getGame().updateDiscardCard(clientGame.getDeck().getDiscardPile().lastElement());
		}
		else {
			clientView.getGame().hideDiscardCard();
		}
	}
	
	/**
	 * Called when the login Button is clicked.
	 * @param view The View that was clicked and called this function
	 */
	public void onButtonLogin(View view) {
		
		//- check the username isn't empty
		final String username = clientGame.getPlayer().getUsername();
		Log.i(ClientGlobal.LOG_INFO, username);
		if (username != null || username != " ") {
			electroService.requestLogin(username);
		}
	}
	
	/**
	 * Called when the start button is clicked.
	 * @param view The View that was clicked and called this function
	 */
	public void onButtonStart(View view) {
		electroService.requestLobbyMessage(JSONComms.serializeLobby(
				JSONLobbyArgs.Status.START));
	}
	
	/**
	 * Called when a Card is clicked, ClientGame should handle this.
	 * @param view The View that was clicked and called this function
	 */
	public void onButtonCard(View view) {
		final GameButton button = (GameButton)view;
		
		//- if the player knocked, end the game
		if (clientGame.getPlayer().getKnocked()) {
			electroService.requestGameMessage(JSONComms.serializeGame(
					JSONGameArgs.State.FINISH,
					JSONGameArgs.Type.ALL,
					clientGame));
			return;
		}
		
		//- if state is discard, discard this card from the pile
		if (clientGame.getState() == JSONGameArgs.State.DISCARD) {
			
			//- add a discard card
			clientGame.getDeck().addDiscardCard(
					clientGame.getPlayer().removeCard(button.getSortCode()));
			
			//- send out a message that this turn is over
			electroService.requestGameMessage(JSONComms.serializeGame(
					JSONGameArgs.State.END,
					JSONGameArgs.Type.ALL,
					clientGame));
		}
	}
	
	/**
	 * Called when the Discard pile is clicked, ClientGame should handle this.
	 * @param view
	 */
	public void onButtonDiscardPile(View view) {
		
		//- if state is play, update accordingly
		if (clientGame.getState() == JSONGameArgs.State.TAKE) {
			clientGame.getPlayer().addCard(clientGame.getDeck().takeDiscardCard());
			
			//- send game message to start DISCARD
			electroService.requestGameMessage(JSONComms.serializeGame(
					JSONGameArgs.State.DISCARD,
					JSONGameArgs.Type.ALL,
					clientGame));
		}
	}
	
	/**
	 * Called when a Play pile is clicked, ClientGame should handle this.
	 * @param view
	 */
	public void onButtonPlayPile(View view) {
		
		//- if state is play, update accordingly
		if (clientGame.getState() == JSONGameArgs.State.TAKE) {
			clientGame.getPlayer().addCard(clientGame.getDeck().takePlayCard());
			
			//- send game message to start DISCARD
			electroService.requestGameMessage(JSONComms.serializeGame(
					JSONGameArgs.State.DISCARD,
					JSONGameArgs.Type.ALL,
					clientGame));
		}
	}
	
	/**
	 * 
	 * @param view
	 */
	public void onButtonKnock(View view) {
		
		//- if the player has taken their turn
		if (clientGame.getState() == JSONGameArgs.State.DISCARD) {
			
			//- has the player called below the deadwood?
			if (clientGame.getPlayer().getDeadwood() <= ClientGlobal.GAME_DEADWOOD_SCORE) {
				
				//- has the player knocked?
				if (!clientGame.getPlayer().getKnocked()) {
					clientGame.getPlayer().setKnocked(true);
					Toast.makeText(
							this,
							getString(R.string.main_game_layout_messages_knock_start),
							Toast.LENGTH_LONG).show();
				}
				else {
					clientGame.getPlayer().setKnocked(false);
					Toast.makeText(
							this,
							getString(R.string.main_game_layout_messages_knock_cancel),
							Toast.LENGTH_SHORT).show();
				}
			}
		}
	}
}