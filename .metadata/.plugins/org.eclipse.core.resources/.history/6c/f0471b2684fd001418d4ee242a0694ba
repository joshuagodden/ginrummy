package com.godden.ginrummy.client.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.godden.ginrummy.R;
import com.godden.ginrummy.interfaces.IDestroyable;

public class GuiLayout extends RelativeLayout implements IDestroyable {

	private StringBuilder _builder;
	
	//- messages
	private TextView _messageWait;
	private TextView _messageStart;
	private TextView _messageDiscard;
	
	public GuiLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public GuiLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public GuiLayout(Context context) {
		super(context);
	}
	
	public void setViews() {
		_messageWait 	= (TextView)findViewById(R.id.main_game_layout_messages_turn_wait);
		_messageStart 	= (TextView)findViewById(R.id.main_game_layout_messages_turn_start);
		_messageDiscard	= (TextView)findViewById(R.id.main_game_layout_messages_turn_discard);
	}
	
	/**
	 * Displays the waiting screen
	 */
	public void displayWaiting() {
		hideAll();
		_messageWait.setVisibility(View.VISIBLE);
	}
	
	/**
	 * Displays the start screen
	 */
	public void displayStart() {
		hideAll();
		_messageStart.setVisibility(View.VISIBLE);
	}
	
	public void displayDiscard() {
		hideAll();
		_messageDiscard.setVisibility(View.VISIBLE);
	}
	
	/**
	 * Disables all the children apart from the Gui itself
	 */
	public void hideAll() {
		int childCount = this.getChildCount();
		for (int i = 0; i < childCount; i++) {
			View v = getChildAt(i);
			v.setVisibility(View.GONE);
			v.setEnabled(false);
		}
	}

	@Override
	public void onDestroy() {
		_builder.setLength(0);
		_builder = null;
	}

}
