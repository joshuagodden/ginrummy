package com.godden.ginrummy.server.json;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;

import com.godden.ginrummy.client.ClientGlobal;

public class JSONLobbyArgs extends JSONArgs {
	
	public enum Status {
		JOIN(0), START(1), ERROR(2);
		private final long value;
		private Status(final long value) {
			this.value = value;
		}
		public long getValue() {
			return value;
		}
		
		/**
		 * Casts a given Long to an enum of Status
		 * @param x	The Long to be converted
		 * @return	Returns an Enum
		 */
		public static Status cast(long x) {
			if (x == 0) { return Status.JOIN; 	}
			if (x == 1) { return Status.START; 	}
			return Status.ERROR;
		}
	}
	
	protected Status status;
	
	/**
	 * Creates and instantiates a new JSON Lobby Arguments
	 * @param json	The JSON to be converted
	 */
	public JSONLobbyArgs(JSONObject json) {
		super(json);
	}

	@Override
	protected void getData() {
		try {
			this.status = Status.cast((Long)json.get("status"));
		} catch (JSONException e) {
			Log.i(ClientGlobal.LOG_JSON, "JSONLobbyArgs failed to get data");)
			e.printStackTrace();
		}
	}

	public Status getState() {
		return status;
	}
}
