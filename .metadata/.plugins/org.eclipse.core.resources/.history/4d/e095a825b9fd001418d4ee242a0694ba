package com.godden.ginrummy.client.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.godden.ginrummy.R;
import com.godden.ginrummy.interfaces.IDestroyable;

public class GuiLayout extends RelativeLayout implements IDestroyable {

	private StringBuilder _builder;
	
	private TextView 	_textDeadwood;
	private TextView 	_textMelds;
	private Button		_buttonKnock;
	
	public GuiLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public GuiLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public GuiLayout(Context context) {
		super(context);
	}
	
	public void setViews() {
		_textDeadwood 	= (TextView)findViewById(R.id.main_game_layout_gui_deadwood);
		_textMelds		= (TextView)findViewById(R.id.main_game_layout_gui_melds);
		_buttonKnock	= (Button)findViewById(R.id.main_game_layout_gui_knock);
	}
	
	/**
	 * Disables all the children apart from the Gui itself
	 */
	public void hideAll() {
		int childCount = this.getChildCount();
		for (int i = 0; i < childCount; i++) {
			View v = getChildAt(i);
			v.setVisibility(View.GONE);
			v.setEnabled(false);
		}
		
		//- update top gui
		_textDeadwood.setVisibility(View.VISIBLE);
		_textMelds.setVisibility(View.VISIBLE);
		_textDeadwood.setEnabled(true);
		_textMelds.setEnabled(true);
		_buttonKnock.setVisibility(View.VISIBLE);
		_buttonKnock.setEnabled(true);
	}
	
	/**
	 * Updates deadwood text
	 * @param deadwood
	 */
	public void updateDeadwood(int deadwood) {
		_textDeadwood.setText("");
		_textDeadwood.setText(this.getContext().getString(R.string.main_game_layout_gui_deadwood) + deadwood);
	}
	
	public void updateDeadwood(String deadwood) {
		_textDeadwood.setText("");
	}
	
	public void updateMelds(String melds) {
		_textMelds.setText("");
		_textMelds.setText(this.getContext().getString(R.string.main_game_layout_gui_melds) + melds);
	}
	
	public void setKnock(boolean value) {
		_buttonKnock.setEnabled(value);
	}

	@Override
	public void onDestroy() {
		_builder.setLength(0);
		_builder = null;
	}
	
	public void displayMessage(int id) {
		Toast.makeText(this.getContext(), this.getContext().getString(id), Toast.LENGTH_LONG).show();
	}

}
