package com.godden.ginrummy.client.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.godden.ginrummy.R;
import com.godden.ginrummy.interfaces.IDestroyable;

public class GuiLayout extends RelativeLayout implements IDestroyable {

	private StringBuilder _builder;
	
	//- messages
	private TextView _messageWait;
	private TextView _messageStart;
	private TextView _messageMend;
	private TextView _messageDiscard;
	
	private Button _buttonOk;
	private Button _buttonCancel;
	
	private Button _buttonGroup;
	private Button _buttonRun;
	
	public GuiLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public GuiLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public GuiLayout(Context context) {
		super(context);
	}
	
	public void setViews() {
		_messageWait 	= (TextView)findViewById(R.id.main_game_layout_messages_turn_wait);
		_messageStart 	= (TextView)findViewById(R.id.main_game_layout_messages_turn_start);
		_messageMend	= (TextView)findViewById(R.id.main_game_layout_messages_turn_cards);
		_messageDiscard	= (TextView)findViewById(R.id.main_game_layout_messages_turn_discard);
		
		_buttonOk 		= (Button)findViewById(R.id.main_game_layout_messages_button_ok);
		_buttonCancel 	= (Button)findViewById(R.id.main_game_layout_messages_button_cancel);
		_buttonGroup 	= (Button)findViewById(R.id.main_game_layout_messages_button_group);
		_buttonRun 		= (Button)findViewById(R.id.main_game_layout_messages_button_run);
	}
	
	/**
	 * Updates the information displayed on the screen about the total number
	 * of runs and groups the player has
	 * @param runs		The runs the player has
	 * @param groups	The groups the player has
	 */
	public void updateCards(int runs, int groups) {
		final TextView cards = (TextView)findViewById(R.id.main_game_layout_messages_turn_cards);
		_builder.setLength(0);
		_builder.append("\n" + getContext().getString(R.string.main_game_layout_messages_turn_runs) + " " + runs);
		_builder.append("\n" + getContext().getString(R.string.main_game_layout_messages_turn_groups) + " " + groups);
		cards.append(_builder.toString());
	}
	
	/**
	 * Displays the waiting screen
	 */
	public void displayWaiting() {
		hideAll();
		_messageWait.setVisibility(View.VISIBLE);
	}
	
	/**
	 * Displays the start screen
	 */
	public void displayStart() {
		hideAll();
		_messageStart.setVisibility(View.VISIBLE);
	}
	
	public void displayMend() {
		hideAll();
		_messageMend.setVisibility(View.VISIBLE);
		
		//- show buttons
		_buttonGroup.setVisibility(View.VISIBLE);
		_buttonGroup.setEnabled(true);
		_buttonRun.setVisibility(View.VISIBLE);
		_buttonRun.setEnabled(true);
	}
	
	public void displayGroupRun() {
		hideAll();
		_buttonOk.setVisibility(View.VISIBLE);
		_buttonOk.setEnabled(true);
		_buttonCancel.setVisibility(View.VISIBLE);
		_buttonCancel.setEnabled(true);
	}
	
	public void displayDiscard() {
		hideAll();
		_messageDiscard.setVisibility(View.VISIBLE);
	}
	
	/**
	 * Disables all the children apart from the Gui itself
	 */
	public void hideAll() {
		int childCount = this.getChildCount();
		for (int i = 0; i < childCount; i++) {
			View v = getChildAt(i);
			v.setVisibility(View.GONE);
			v.setEnabled(false);
		}
	}

	@Override
	public void onDestroy() {
		_builder.setLength(0);
		_builder = null;
	}

}
