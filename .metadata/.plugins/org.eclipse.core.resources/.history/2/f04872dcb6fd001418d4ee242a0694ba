package com.godden.ginrummy.server.json;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;

import android.util.Log;

import com.godden.ginrummy.client.ClientGlobal;

public class JSONGameArgs extends JSONArgs {
	
	/**
	 * State based enum for gameplay
	 * @author Joshua Godden
	 */
	public enum State {
		CREATE(0), START(1), TAKE(2), DISCARD(3), END(4), FINISH(5), DRAW(6), ERROR(7);
		private final int value;
		private State(final int value) {
			this.value = value;
		}
		public int getValue() {
			return value;
		}
		
		/**
		 * Casts a given long (JSON) to an enum of State
		 * @param x	The long to be converted
		 * @return	Returns an Enum
		 */
		public static State cast(int x) {
			if (x == 0) { return State.CREATE; 	}
			if (x == 1) { return State.START; 	}
			if (x == 2) { return State.TAKE;	}
			if (x == 3) { return State.DISCARD;	}
			if (x == 4) { return State.END;		}
			if (x == 5) { return State.FINISH;	}
			if (x == 6) { return State.DRAW;	}
			return State.ERROR;
		}
	}
	
	/**
	 * Who the message type is for
	 * @author Joshua Godden
	 */
	public enum Type {
		DEALER(0), ALL(1), ERROR(2);
		private final int value;
		private Type(final int value) {
			this.value = value;
		}
		public int getValue() {
			return value;
		}
		
		/**
		 * Casts a given int to an enum of Type
		 * @param x	The int to be converted
		 * @return	Returns an Enum
		 */
		public static Type cast(int x) {
			if (x == 0) { return Type.DEALER; 	}
			if (x == 1) { return Type.ALL; 		}
			return Type.ERROR;
		}
	}
	
	public Type type;
	public State state;
	public JSONArray playerOne;
	public JSONArray playerTwo;
	public JSONArray playPile;
	public JSONArray discardPile;
	public int winningScore;
	
	/**
	 * Creates and instantiates a new JSON Game Arguments
	 * @param json	The JSON to be converted
	 */
	public JSONGameArgs(JSONObject json) {
		super(json);
	}

	@Override
	protected void getData() {
		try {
			type 				= Type.cast(json.getInt("type"));
			state 				= State.cast(json.getInt("state"));
			JSONObject gameData = json.getJSONObject("gameData");
			
			/*
			 * Using the follow through enum technique. Don't need to use so many if's, can just let
			 * the JSON extract out as required.
			 */
			switch (state) {
				case START		:
						playerOne = gameData.getJSONArray("playerOne");
						playerTwo = gameData.getJSONArray("playerTwo");
				case TAKE		:
				case DISCARD	:
				case FINISH		:
						winningScore = gameData.getInt("winningScore");
				case END		:
						playPile 	= gameData.getJSONArray("playPile");
						discardPile = gameData.getJSONArray("discardPile");
						break;
				default			:
					break;
			}
		}
		catch (JSONException e) {
			Log.i(ClientGlobal.LOG_JSON, "JSONGameArgs failed to get data for " + e.getMessage());
			e.printStackTrace();
		}
	}
	
	public Type getType() {
		return type;
	}
	
	public State getState() {
		return state;
	}
}
