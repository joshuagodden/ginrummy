package com.godden.ginrummy.server;

import java.util.ArrayList;

import com.electrotank.electroserver5.client.ElectroServer;
import com.electrotank.electroserver5.client.api.EsConnectionResponse;
import com.electrotank.electroserver5.client.api.EsCreateRoomRequest;
import com.electrotank.electroserver5.client.api.EsJoinRoomEvent;
import com.electrotank.electroserver5.client.api.EsLoginRequest;
import com.electrotank.electroserver5.client.api.EsLoginResponse;
import com.electrotank.electroserver5.client.api.EsMessageType;
import com.electrotank.electroserver5.client.api.EsPublicMessageEvent;
import com.electrotank.electroserver5.client.api.EsPublicMessageRequest;
import com.electrotank.electroserver5.client.connection.AvailableConnection;
import com.electrotank.electroserver5.client.connection.TransportType;
import com.electrotank.electroserver5.client.server.Server;
import com.electrotank.electroserver5.client.user.User;
import com.electrotank.electroserver5.client.zone.Room;
import com.godden.ginrummy.R;
import com.godden.ginrummy.client.ClientGlobal;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

/**
 * An extended Service that implements the ElectroServer library. Allows the creation of a room, connecting to a room
 * and sending messages through the server. Any activity that binds to this service will recieve the public intent messages
 * from this service (and ElectroServer).
 * @author Joshua Godden
 * @version 1.0
 */
public class ElectroService extends Service {
	
	//- MESSAGE TYPES
	public static final String NEW_SERVER_MESSAGE 	= "New_Server_Message";
	public static final String NEW_PUBLIC_MESSAGE 	= "New_Public_Message";
	public static final String NEW_GAME_MESSAGE 	= "New_Game_Message";
	public static final String NEW_LOBBY_MESSAGE	= "New_Lobby_Message";
	public static final String NEW_MESSAGE_DELIM	= "~";
	
	//- OUTPUT TYPES
	public static final String SERVER_NAME = "server1";
	
	//== ELECTROSERVER ==================================
	private Server 				_server;
	private Room				_serverRoom;
	private AvailableConnection _connection;
	private ElectroServer 		_electroServer;
	private boolean				_registered;
	//===================================================
	
	//== BINDER =========================================
	private final IBinder _binder = new LocalBinder(this);
	//===================================================
	
	//== CONSTRUCTORS ===============================================
	/**
	 * Creates and instantiates a new ConnectionService
	 */
	public ElectroService() {
		_registered = false;
	}
	//===============================================================
	
	//== OVERRIDES ==================================================
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.i(ClientGlobal.LOG_SERVICE, "ElectroServer -> Service started");
		
		//- setup server
		_electroServer 	= new ElectroServer();
		_registered 	= true;
		
		//- setup event listeners
		_electroServer.getEngine().addEventListener(
				EsMessageType.ConnectionResponse,
				this,
				"onEventConnectionResponse",
				EsConnectionResponse.class);
		_electroServer.getEngine().addEventListener(
				EsMessageType.LoginResponse,
				this,
				"onEventLoginResponse",
				EsLoginResponse.class);
		_electroServer.getEngine().addEventListener(
				EsMessageType.JoinRoomEvent,
				this,
				"onEventJoinRoom",
				EsJoinRoomEvent.class);
		_electroServer.getEngine().addEventListener(
				EsMessageType.PublicMessageEvent,
				this,
				"onEventPublicMessage",
				EsPublicMessageEvent.class);
		
		//- fire off thread seperate from UI
		Thread t = new Thread() {
			public void run() {
				try {
					loadServer();
					connectServer();
				}
				catch (Exception ex) {
				}
			}
		};
		t.start();
		return Service.START_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent) {
		return _binder;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (_registered) {
			
		}
		Log.i(ClientGlobal.LOG_SERVICE, "ElectroServer -> Service destroyed");
	}
	//===============================================================
	
	//== GETTERS ====================================================
	/**
	 * Gets the room the server is currently using
	 * @return
	 */
	public Room getRoom() {
		return _serverRoom;
	}
	
	/**
	 * Gets the server the service is using
	 * @return
	 */
	public Server getServer() {
		return _server;
	}
	//===============================================================
	
	//== FUNCTIONS ==================================================
	/**
	 * Loads the server
	 */
	private void loadServer() {
		Log.i(ClientGlobal.LOG_SERVICE, "ElectroServer -> Creating server");
		
		_server = new Server(SERVER_NAME);
		_connection = new AvailableConnection(
				getBaseContext().getString(R.string.es_address_ip),
				getResources().getIntArray(R.array.es_ports)[0],
				TransportType.BinaryTCP);
		_server.addAvailableConnection(_connection);
		_electroServer.getEngine().addServer(_server);
		Log.i(ClientGlobal.LOG_SERVICE, "ElectroServer -> Adding server");
	}
	
	/**
	 * Connects to the server
	 */
	private void connectServer() {
		_electroServer.getEngine().connect(_server, _connection);
		Log.i(ClientGlobal.LOG_SERVICE, "ElectroServer -> Requesting connect");
	}
	
	/**
	 * Closes the connection
	 */
	public void closeConnection() {
		
		//- remove event listeners
		_electroServer.getEngine().removeEventListener(
				EsMessageType.ConnectionResponse,
				this,
				"onEventConnectionResponse");
		_electroServer.getEngine().removeEventListener(
				EsMessageType.LoginResponse,
				this,
				"onEventLoginResponse");
		_electroServer.getEngine().removeEventListener(
				EsMessageType.JoinRoomEvent,
				this,
				"onEventJoinRoom");
		_electroServer.getEngine().removeEventListener(
				EsMessageType.PublicMessageEvent,
				this,
				"onEventPublicMessage");
		
		//- close server
		_electroServer.getEngine().close();
		Log.i(ClientGlobal.LOG_SERVICE, "ElectroServer -> Closing server");
	}
	
	/**
	 * Request to join a room
	 */
	public void requestCreateRoom() {
		Log.i(ClientGlobal.LOG_SERVICE, "ElectroServer -> Requesting room create");
		EsCreateRoomRequest crr = new EsCreateRoomRequest();
		crr.setRoomName(getBaseContext().getString(R.string.es_address_room));
		crr.setZoneName(getBaseContext().getString(R.string.es_address_zone));
		crr.setUsingLanguageFilter(true);
		crr.setReceivingVideoEvents(false);
		_electroServer.getEngine().send(crr);
	}
	
	/**
	 * Request a login
	 * @param username	The players username
	 */
	public void requestLogin(String username) {
		Log.i(ClientGlobal.LOG_SERVICE, "ElectroServer -> Requesting login");
		
		//- if we are connected
		if (_electroServer.getEngine().isConnected()) {
			
			if (username == null || username.length() < 1) {
				username = "DefaultUser" + Math.round(10000 * Math.random());
			}
			
			EsLoginRequest lr = new EsLoginRequest();
			lr.setUserName(username);
			_electroServer.getEngine().send(lr);
			
			Log.i(ClientGlobal.LOG_SERVICE, "- Username: " + username);
		}
		else {
			Log.e(ClientGlobal.LOG_SERVICE, "- Not connected to the engine. Retry login");
		}
	}
	
	/**
	 * Request a public message to be sent to the server
	 * @param message	The message to be sent
	 */
	public void requestPublicMessage(String tag, String message) {
		Log.i(ClientGlobal.LOG_SERVICE, "ElectroServer -> Requesting public message");
		
		//- check the tag isnt empty
		if (tag == null || tag.length() < 1) {
			Log.w(ClientGlobal.LOG_SERVICE, "Tag is empty, using Public tag");
			tag = NEW_PUBLIC_MESSAGE;
		}
		Log.i(ClientGlobal.LOG_SERVICE, "- Tag: " + tag);
		
		//- create a public message, with our Room and Zone ID's, then send it
		EsPublicMessageRequest spmr = new EsPublicMessageRequest();
		spmr.setRoomId(_serverRoom.getId());
		spmr.setZoneId(_serverRoom.getZoneId());
		spmr.setMessage(tag + NEW_MESSAGE_DELIM + message);
		_electroServer.getEngine().send(spmr);
	}
	
	/**
	 * Handy function for sending a game message
	 * @param message	The message to send
	 */
	public void requestGameMessage(String message) {
		requestPublicMessage(NEW_GAME_MESSAGE, message);
	}
	
	/**
	 * Handy function for sending a lobby message
	 * @param message	The message to send
	 */
	public void requestLobbyMessage(String message) {
		requestPublicMessage(NEW_LOBBY_MESSAGE, message);
	}
	//===============================================================
	
	
	//== EVENTS =====================================================
	public void onEventConnectionResponse(EsConnectionResponse e) {
		Log.w(ClientGlobal.LOG_SERVICE, "ElectroServer -> Event Dispatched: Connection Response");
		
		//- check connection
		if (e.isSuccessful())	{ outputServerConnectedOk(); 	} 
		else					{ outputServerConnectedFail(); 	}
	}
	
	public void onEventLoginResponse(EsLoginResponse e) {
		Log.w(ClientGlobal.LOG_SERVICE, "ElectroServer -> Event Dispatched: Login Response");
		
		//- check login
		if (e.isSuccessful())	{ outputServerLoginOk(e.getUserName().toString());		}
		else					{ outputServerLoginFail(e.getError().toString());		}
	}
	
	public void onEventJoinRoom(EsJoinRoomEvent e) {
		Log.w(ClientGlobal.LOG_SERVICE, "ElectroServer -> Event Dispatched: Join Response");
		short roomID 	= (short)e.getRoomId();
		short zoneID 	= (short)e.getZoneId();
		_serverRoom 	= _electroServer.getManagerHelper().getZoneManager().zoneById(zoneID).roomById(roomID);
		outputServerRoomOk();
	}
	
	public void onEventPublicMessage(EsPublicMessageEvent e) {
		Log.w(ClientGlobal.LOG_SERVICE, "ElectroServer -> Event Dispatched: Public Message");
		
		//- get the message
		String[] m 		= e.getMessage().split(NEW_MESSAGE_DELIM);
		String tag 		= m[0].toString();
		String message 	= m[1].toString();
		Log.w(ClientGlobal.LOG_SERVICE, "- Tag		: " + tag);
		Log.w(ClientGlobal.LOG_SERVICE, "- User		: " + e.getUserName());
		Log.w(ClientGlobal.LOG_SERVICE, "- Message	: " + message);
		outputPublicMessage(tag, e.getUserName(), message);
	}
	//===============================================================
	
	//== OUTPUT =====================================================
	protected void outputServerConnectedOk() {
		Log.w(ClientGlobal.LOG_SERVICE, "- Connection successfull");
		Intent intent = new Intent(NEW_SERVER_MESSAGE);
		intent.putExtra("type", "connected");
		sendBroadcast(intent);
	}
	
	protected void outputServerConnectedFail() {
		Log.w(ClientGlobal.LOG_SERVICE, "- Connection failed");
		Intent intent = new Intent(NEW_SERVER_MESSAGE);
		intent.putExtra("type", "disconnected");
		sendBroadcast(intent);
	}
	
	protected void outputServerLoginOk(String username) {
		Log.w(ClientGlobal.LOG_SERVICE, "- Login successfull");
		Intent intent = new Intent(NEW_SERVER_MESSAGE);
		intent.putExtra("type", "login");
		intent.putExtra("username", username);
		sendBroadcast(intent);
	}
	
	protected void outputServerLoginFail(String error) {
		Log.w(ClientGlobal.LOG_SERVICE, "- Login fail");
		Intent intent = new Intent(NEW_SERVER_MESSAGE);
		intent.putExtra("type", "error");
		intent.putExtra("error", error);
		sendBroadcast(intent);
	}
	
	protected void outputServerRoomOk() {
		Log.w(ClientGlobal.LOG_SERVICE, "- Room Join successfull");
		Intent intent = new Intent(NEW_SERVER_MESSAGE);
		intent.putExtra("type", "join");
		sendBroadcast(intent);
	}
	
	protected void outputPublicMessage(String tag, String user, String message) {
		Intent intent 	= new Intent(tag);
		intent.putExtra("user", 	user);
		intent.putExtra("tag", 		tag);
		intent.putExtra("message", 	message);
		sendBroadcast(intent);
	}
	//===============================================================
	
	//== GETTERS ====================================================
	/**
	 * Gets all the users from the current server room
	 * @return	Returns an converted Collection to ArrayList of all users in the current room. Returns null if the Room is empty
	 */
	public ArrayList<User> getUsers() {
		if (_serverRoom != null) {
			return new ArrayList<User>(_serverRoom.getUsers());
		}
		return null;
	}
	
	/**
	 * Gets the total number of users
	 * @return The number of players in a room
	 */
	public int getUserSize() {
		if (_serverRoom != null) {
			return _serverRoom.getUsers().size();
		}
		return 0;
	}
	//===============================================================
}
