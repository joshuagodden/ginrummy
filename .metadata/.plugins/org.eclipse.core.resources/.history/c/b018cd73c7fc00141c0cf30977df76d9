package com.godden.ginrummy.client;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Stack;

/**
 * Representation of the deck within the game
 * @author Joshua Godden
 */
public final class ClientDeck {
	
	/**
	 * An array of available to play cards. Using stack for last in, first out
	 */
	protected Stack<ClientCard> playPile;
	
	/**
	 * An array of available discard cards. Using stack for last in, first out
	 */
	protected Stack<ClientCard> discardPile;
	
	/**
	 * Creates and instantiates a new Deck system
	 */
	public ClientDeck() {
		playPile 	= new Stack<ClientCard>();
		discardPile = new Stack<ClientCard>();
	}
	
	/**
	 * Sets up a new Play pile
	 */
	public void create() {
		
		//- get our global setup, and start adding to the play pile, which needs to be shuffled
		for (int i = 0; i < ClientGlobal.GAME_CARDS.length; i++) {
			final String[] card = ((String)ClientGlobal.GAME_CARDS[i]).split(";");
			
			//- parse the card
			playPile.add(new ClientCard(
					Integer.parseInt(card[0]),
					Integer.parseInt(card[1]),
					Integer.parseInt(card[2]),
					Integer.parseInt(card[3]),
					Integer.parseInt(card[4])));
		}
		
		//- when its all done, shuffle with the Collections method
		Collections.shuffle(playPile);
	}
	
	/**
	 * Removes a card from the play pile and returns it
	 * @return	Returns a play card
	 */
	public ClientCard takePlayCard() {
		return playPile.pop();
	}
	
	/**
	 * Removes a card from the discard pile and returns it
	 * @return	Returns a discard card
	 */
	public ClientCard takeDiscardCard() {
		return discardPile.pop();
	}
	
	/**
	 * Adds a card to the play pile
	 * @param card The card to add
	 */
	public void addPlayCard(ClientCard card) {
		playPile.add(card);
	}
	
	/**
	 * Adds a card to the discard pile
	 * @param card	The card to add
	 */
	public void addDiscardCard(ClientCard card) {
		discardPile.add(card);
		
	}
	
	public ArrayList<String> getPlayPileJSON() {
		ArrayList<String> pile = new ArrayList<String>();
		for (int i = 0; i < playPile.size(); i++) {
			pile.add(playPile.get(i).toString());
		}
	}
	
	public ArrayList<String> getDiscardPileJSON() {
		
	}
	
	/**
	 * Gets the play pile
	 * @return Returns a Stack with the playable cards
	 */
	public Stack<ClientCard> getPlayPile() {
		return playPile;
	}
	
	/**
	 * Gets the discard pile
	 * @return Returns a Stack with the discardable cards
	 */
	public Stack<ClientCard> getDiscardPile() {
		return discardPile;
	}
}
